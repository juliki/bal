import { Injectable }              from '@angular/core';
import { Http }          from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class HomeService {

    private devUrl: string = 'https://www.banklenz.de/navigation_items/kunden-app/current_version/1/dev/1/'; // URL to DEV API
    private homeUrl: string = 'http://www.banklenz.de/navigation_items/kunden-app/current_version/1/'; // URL to PROD API

    public currentMode: string = 'public mode';
    public condition: boolean = true;

    constructor(private http: Http) {

    }

    getMenuItems() {

        let url = null;

        if(this.currentMode == 'public mode'){
            this.condition = true;
            url = this.homeUrl;
        }else{
            url = this.devUrl;
            this.condition = false;
        }

        return this.http.get(url)
                        .toPromise()
                        .then(response => response.json());
    }


}
