import { Injectable } from '@angular/core';
import { Geolocation } from 'ionic-native';

import { LaunchNavigator, LaunchNavigatorOptions } from 'ionic-native';
import { NgZone } from '@angular/core';

declare var google;

@Injectable()
export class GoogleMaps {

  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  markers: any = [];
  apiKey: string = 'AIzaSyAG4j_eYI_l8Z_ESP042yrDy_d0BUvdgNU';
  infowindow: any;
  currentPosition_lat: any;
  currentPosition_lng: any;
  balOnly: boolean = false;
  currentMapLatLng: any = null;
  currentZoom: any = null;

  constructor(public ngZone: NgZone) {
    window['ionicPageRef'] = {
        zone: this.ngZone,
        component: this
    };

  }

  init(mapElement: any, pleaseConnect: any): Promise<any> {

    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps();

  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if(typeof google == "undefined" || typeof google.maps == "undefined"){

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();


          window['mapInit'] = () => {

            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          }

          let script = document.createElement("script");
          script.id = "googleMaps";

          if(this.apiKey){
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);

        
      }
      else {
          this.initMap().then((res)=>{
            this.enableMap();
            resolve(true);
          });
      }

      //this.addConnectivityListeners();

    });

  }

  initMap(): Promise<any> {

    this.mapInitialised = true;

    return new Promise((resolve) => {

      Geolocation.getCurrentPosition().then((position) => {

        //Set initial center position
        let latLng;
        if(this.currentMapLatLng){
          latLng = this.currentMapLatLng;
        }else{
          latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        }

        //Set initial zoom state
        let currentZoom;
        if(this.currentZoom){
          currentZoom = this.currentZoom;
        }else{
          currentZoom = 15;
        }


        this.currentPosition_lat = position.coords.latitude;
        this.currentPosition_lng = position.coords.longitude;


        let mapOptions = {
          center: latLng,
          zoom: currentZoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);

        var noShow = [
          {
              featureType: "poi",
              stylers: [
                { visibility: "off" }
              ]
            },
            {
                featureType: "transit",
                stylers: [
                  { visibility: "off" }
                ]
              }
          ];

          this.map.setOptions({styles: noShow});

        //Init infowindow
        this.infowindow = new google.maps.InfoWindow({});

        resolve(true);

      });

    });



  }

  disableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "block";
    }

  }

  enableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "none";
    }

  }

  addConnectivityListeners(): void {

    document.addEventListener('online', () => {

      console.log("online");

      setTimeout(() => {

        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        }
        else {
          if(!this.mapInitialised){
            this.initMap();
          }

          this.enableMap();
        }

      }, 2000);

    }, false);

    document.addEventListener('offline', () => {

      console.log("offline");

      this.disableMap();

    }, false);

  }

  addMarker(lat: number, lng: number, balOnly: boolean, address: any): void {

    let markerIcon = 'assets/icons/map_pin_nonbal_atms_small.svg';

    if (balOnly){
      markerIcon = 'assets/icons/map_pin_bal_atms_small.svg';
    }

    let latLng = new google.maps.LatLng(lat, lng);

    let marker = new google.maps.Marker({
      map: this.map,
      position: latLng,
      icon: markerIcon
    });



     marker.addListener('click', () => {

       //Setting up template of InfoWindow

       if(balOnly){
          var contentString = '<h2>' + address.CompanyName + '</h2><span>' + address.CompanyNameAddon1 + '</span><br> <span>' + address.Street + '</span> <br> <span>'+ address.Zip + ' ' + address.City +' </span><br><button onClick="window.ionicPageRef.zone.run(() => { window.ionicPageRef.component.openNavigatorApp(' +lat+','+ lng + ','+ this.currentPosition_lat+','+ this.currentPosition_lng +') })">Route anzeigen</button>';
       } else{
          var contentString = '<h2>' + address.CompanyName + '</h2><span>' + address.Street + '</span> <br> <span>'+ address.Zip + ' ' + address.City +' </span><br><button onClick="window.ionicPageRef.zone.run(() => { window.ionicPageRef.component.openNavigatorApp(' +lat+','+ lng + ','+ this.currentPosition_lat+','+ this.currentPosition_lng +') })">Route anzeigen</button>';
       }

       //Setting content to existed infowindow object with specific data
       this.infowindow.setContent(contentString);

       //Openening info windw
       this.infowindow.open(this.map, marker);

     });

    this.markers.push(marker);

  }

  addSimpleMarker(lat: number, lng: number, balOnly: boolean): void {

      let markerIcon = 'assets/icons/map_pin_nonbal_atms_small.svg';

      if (balOnly){
        markerIcon = 'assets/icons/map_pin_bal_atms_small.svg';
      }

      let latLng = new google.maps.LatLng(lat, lng);

      let marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon: markerIcon
      });

      this.markers.push(marker);

  }

  openNavigatorApp(lat: any, lng: any, position_lat: any, position_lng: any){


    console.log("nam method Launched!!!");

    let destination = [lat, lng];
    let currentPosition = position_lat+','+ position_lng;
    let navOptions: LaunchNavigatorOptions = {
          start: currentPosition
       };

    console.log(destination, navOptions);


    LaunchNavigator.navigate(destination, navOptions)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  getMyLocation(): Promise<any> {

    return new Promise((resolve) => {

      Geolocation.getCurrentPosition().then((position) => {

        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.panTo(latLng);

        resolve(true);

      });

    });



  }

  getMapCenter(): Promise<any>{
    return new Promise((resolve) => {

      let mapCenterCoords = {
        lat: this.map.getCenter().lat(),
        lng: this.map.getCenter().lng()
      };

      resolve(mapCenterCoords);
    });
  }

  removeMarkers(){
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }



}
