import { Injectable } from '@angular/core';
import { Connectivity } from './connectivity';
import { Geolocation } from 'ionic-native';
import { GoogleAnalytics } from 'ionic-native';

import { LaunchNavigator, LaunchNavigatorOptions } from 'ionic-native';
import { NgZone } from '@angular/core';


declare var google;

@Injectable()
export class GoogleMaps {

  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  markers: any = [];
  apiKey: string = 'AIzaSyAG4j_eYI_l8Z_ESP042yrDy_d0BUvdgNU';
  infowindow: any;
  currentPosition_lat: any;
  currentPosition_lng: any;
  balOnly: boolean = false;
  currentMapLatLng: any = null;
  currentZoom: any = null;
  locationIndicator: any;

  //For details page
  mapElement_details: any;
  pleaseConnect_details: any;
  map_details: any;
  mapInitialised_details: boolean = false;
  mapLoaded_details: any;
  mapLoadedObserver_details: any;
  currentPosition_lat_details: any;
  currentPosition_lng_details: any;
  currentMapLatLng_details: any = null;
  currentZoom_details: any = null;

  constructor(public connectivityService: Connectivity, public ngZone: NgZone) {
    window['ionicPageRef'] = {
        zone: this.ngZone,
        component: this
    };

  }

  init(mapElement: any, pleaseConnect: any): Promise<any> {

    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps();

  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if(typeof google == "undefined" || typeof google.maps == "undefined"){

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if(this.connectivityService.isOnline()){

          window['mapInit'] = () => {

            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          }

          let script = document.createElement("script");
          script.id = "googleMaps";

          if(this.apiKey){
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);

        }
      }
      else {

        if(this.connectivityService.isOnline()){
          this.initMap().then((res)=>{
            this.enableMap();
            resolve(true);
          });

        }
        else {
          this.disableMap();
        }

      }

      this.addConnectivityListeners();

    });

  }

  initMap(): Promise<any> {

    this.mapInitialised = true;

    return new Promise((resolve) => {

      Geolocation.getCurrentPosition({timeout: 5000}).then((position) => {

        //Set initial center position
        let latLng;
        if(this.currentMapLatLng){
          latLng = this.currentMapLatLng;
        }else{
          latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        }

        console.log("Coord are: "+position.coords.latitude +','+position.coords.longitude);


        //Set initial zoom state
        let currentZoom;
        if(this.currentZoom){
          currentZoom = this.currentZoom;
        }else{
          currentZoom = 15;
        }


        this.currentPosition_lat = position.coords.latitude;
        this.currentPosition_lng = position.coords.longitude;


        let mapOptions = {
          center: latLng,
          zoom: currentZoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);

        var noShow = [
          {
              featureType: "poi",
              stylers: [
                { visibility: "off" }
              ]
            },
            {
                featureType: "transit",
                stylers: [
                  { visibility: "off" }
                ]
              }
          ];

          this.map.setOptions({styles: noShow});

        //Init infowindow
        this.infowindow = new google.maps.InfoWindow({});

        //Set My location Indicator
        this.setMyLocationIndicator(position.coords.latitude, position.coords.longitude, false);

        resolve(true);

      }).catch((error) => {
        console.log('Error getting location', error);

        //Set initial center position to Munich
        let latLng = new google.maps.LatLng(48.163731, 11.54318);

        //Set initial zoom state
        let currentZoom;
        if(this.currentZoom){
          currentZoom = this.currentZoom;
        }else{
          currentZoom = 15;
        }

        this.currentPosition_lat = 48.163731;
        this.currentPosition_lng = 11.54318;


        let mapOptions = {
          center: latLng,
          zoom: currentZoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);

        var noShow = [
          {
              featureType: "poi",
              stylers: [
                { visibility: "off" }
              ]
            },
            {
                featureType: "transit",
                stylers: [
                  { visibility: "off" }
                ]
              }
          ];

          this.map.setOptions({styles: noShow});

        //Init infowindow
        this.infowindow = new google.maps.InfoWindow({});


        resolve(true);
      });

    });



  }

  disableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "block";
    }

  }

  enableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "none";
    }

  }

  addConnectivityListeners(): void {

    document.addEventListener('online', () => {

      console.log("online");

      setTimeout(() => {

        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        }
        else {
          if(!this.mapInitialised){
            this.initMap();
          }

          this.enableMap();
        }

      }, 2000);

    }, false);

    document.addEventListener('offline', () => {

      console.log("offline");

      this.disableMap();

    }, false);

  }

  addMarker(lat: number, lng: number, balOnly: boolean, address: any): void {

    let markerIcon = 'assets/icons/map_pin_nonbal_atms_small.svg';

    if (balOnly){
      markerIcon = 'assets/icons/map_pin_bal_atms_small.svg';
    }

    let latLng = new google.maps.LatLng(lat, lng);

    let marker = new google.maps.Marker({
      map: this.map,
      position: latLng,
      icon: markerIcon
    });



     marker.addListener('click', () => {
       //GoogleAnalytics
       GoogleAnalytics.trackEvent("Map Event", "View info window", address.CompanyName+', '+address.Street+', '+address.City).then(() => {
         console.log('InfoWindow open Tracked');
       });

       //Setting up template of InfoWindow

       //Called from param for GoogleAnalytics
       let calledFrom= "'Map Event'";

       if(balOnly){
          var contentString = '<h2>' + address.CompanyName + '</h2><span>' + address.CompanyNameAddon1 + '</span><br> <span>' + address.Street + '</span> <br> <span>'+ address.Zip + ' ' + address.City +' </span><br><button onClick="window.ionicPageRef.zone.run(() => { window.ionicPageRef.component.openNavigatorApp(' +lat+','+ lng + ','+ this.currentPosition_lat+','+ this.currentPosition_lng +',' + calledFrom +') })">Route anzeigen</button>';
       } else{
          var contentString = '<h2>' + address.CompanyName + '</h2><span>' + address.Street + '</span> <br> <span>'+ address.Zip + ' ' + address.City +' </span><br><button onClick="window.ionicPageRef.zone.run(() => { window.ionicPageRef.component.openNavigatorApp(' +lat+','+ lng + ','+ this.currentPosition_lat+','+ this.currentPosition_lng +','+ calledFrom +') })">Route anzeigen</button>';
       }

       //Setting content to existed infowindow object with specific data
       this.infowindow.setContent(contentString);

       //Openening info windw
       this.infowindow.open(this.map, marker);

     });

    this.markers.push(marker);

  }

  setMyLocationIndicator(lat: number, lng: number, details: boolean): void {

    //Clear refference before loading Marker
    if(this.locationIndicator){
      this.locationIndicator.setMap(null);
    }    

    let markerIcon = new google.maps.MarkerImage('assets/icons/mobileimgs2.png',
                                                    new google.maps.Size(22,22),
                                                    new google.maps.Point(0,18),
                                                    new google.maps.Point(11,11));

    let latLng = new google.maps.LatLng(lat, lng);
    
    // Select a map to put indicator on
    let map = this.map;

    // If details param true - select the detyails map instance
    if (details === true){
      map = this.map_details;
    }

    let marker = new google.maps.Marker({
      map: map,
      position: latLng,
      icon: markerIcon,
      shadow: null,
      zIndex: 999,
      clickable: false
    });

    //Set refference handler
    this.locationIndicator = marker;

  }

  /** ***************************
   * For details page start
  **/

  init_details(mapElement: any, pleaseConnect: any): Promise<any> {

    this.mapElement_details = mapElement;
    this.pleaseConnect_details = pleaseConnect;

    return this.loadGoogleMaps_details();

  }

  loadGoogleMaps_details(): Promise<any> {

    return new Promise((resolve) => {

      if(typeof google == "undefined" || typeof google.maps == "undefined"){

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap_details();

        if(this.connectivityService.isOnline()){

          window['mapInit'] = () => {

            this.initMap_details().then(() => {
              resolve(true);
            });

            this.enableMap_details();
          }

          let script = document.createElement("script");
          script.id = "googleMaps";

          if(this.apiKey){
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);

        }
      }
      else {

        if(this.connectivityService.isOnline()){
          this.initMap_details().then((res)=>{
            this.enableMap_details();
            resolve(true);
          });

        }
        else {
          this.disableMap_details();
        }

      }

      this.addConnectivityListeners_details();

    });

  }

  initMap_details(): Promise<any> {

    this.mapInitialised_details = true;

    return new Promise((resolve) => {

      Geolocation.getCurrentPosition({timeout: 5000}).then((position) => {

        //Set initial center position
        let latLng;
        if(this.currentMapLatLng_details){
          latLng = this.currentMapLatLng_details;
        }else{
          latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        }

        //Set initial zoom state
        let currentZoom;
        if(this.currentZoom_details){
          currentZoom = this.currentZoom_details;
        }else{
          currentZoom = 15;
        }


        this.currentPosition_lat_details = position.coords.latitude;
        this.currentPosition_lng_details = position.coords.longitude;


        let mapOptions = {
          center: latLng,
          zoom: currentZoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map_details = new google.maps.Map(this.mapElement_details, mapOptions);

        var noShow = [
          {
              featureType: "poi",
              stylers: [
                { visibility: "off" }
              ]
            },
            {
                featureType: "transit",
                stylers: [
                  { visibility: "off" }
                ]
              }
          ];

          this.map_details.setOptions({styles: noShow});

        //Set My location Indicator
        this.setMyLocationIndicator(position.coords.latitude, position.coords.longitude, true);

        resolve(true);

      }).catch((error) => {
        console.log('Error getting location', error);

        //Set initial center position to Munich
        let latLng = new google.maps.LatLng(48.163731, 11.54318);

        //Set initial zoom state
        let currentZoom;
        if(this.currentZoom_details){
          currentZoom = this.currentZoom_details;
        }else{
          currentZoom = 15;
        }

        this.currentPosition_lat_details = 48.163731;
        this.currentPosition_lng_details = 11.54318;


        let mapOptions = {
          center: latLng,
          zoom: currentZoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map_details = new google.maps.Map(this.mapElement_details, mapOptions);

        var noShow = [
          {
              featureType: "poi",
              stylers: [
                { visibility: "off" }
              ]
            },
            {
                featureType: "transit",
                stylers: [
                  { visibility: "off" }
                ]
              }
          ];

          this.map_details.setOptions({styles: noShow});

        resolve(true);
      });;

    });



  }

  disableMap_details(): void {

    if(this.pleaseConnect_details){
      this.pleaseConnect_details.style.display = "block";
    }

  }

  enableMap_details(): void {

    if(this.pleaseConnect_details){
      this.pleaseConnect_details.style.display = "none";
    }

  }

  addConnectivityListeners_details(): void {

    document.addEventListener('online', () => {

      console.log("online");

      setTimeout(() => {

        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps_details();
        }
        else {
          if(!this.mapInitialised_details){
            this.initMap_details();
          }

          this.enableMap_details();
        }

      }, 2000);

    }, false);

    document.addEventListener('offline', () => {

      console.log("offline");

      this.disableMap_details();

    }, false);

  }

  addSimpleMarker(lat: number, lng: number, balOnly: boolean): void {

      let markerIcon = 'assets/icons/map_pin_nonbal_atms_small.svg';

      if (balOnly){
        markerIcon = 'assets/icons/map_pin_bal_atms_small.svg';
      }

      let latLng = new google.maps.LatLng(lat, lng);

      let marker = new google.maps.Marker({
        map: this.map_details,
        position: latLng,
        icon: markerIcon
      });

      //this.markers.push(marker);

  }




  /*****************************
   * For details page end
   */

  openNavigatorApp(lat: any, lng: any, position_lat: any, position_lng: any, calledFrom: string){

    //GoogleAnalytics
    GoogleAnalytics.trackEvent(calledFrom, "Calculate route", lat+', '+lng).then(() => {
      console.log('navigator Launch Tracked at - '+ calledFrom+' , '+ lat+','+lng);
    });


    let destination = [lat, lng];
    let currentPosition = position_lat+','+ position_lng;
    let navOptions: LaunchNavigatorOptions = {
          start: currentPosition
       };

    console.log(destination, navOptions);


    LaunchNavigator.navigate(destination, navOptions)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  getMyLocation(): Promise<any> {

    return new Promise((resolve) => {

      Geolocation.getCurrentPosition({timeout: 5000}).then((position) => {

        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.panTo(latLng);

        //Set My location Indicator
        this.setMyLocationIndicator(position.coords.latitude, position.coords.longitude, false);

        //Set zoom
        this.map.setZoom(17);

        resolve(true);

      }).catch((error) => {
        console.log('Error getting location', error);
      });

    });



  }

  getMapCenter(): Promise<any>{
    return new Promise((resolve) => {

      let mapCenterCoords = {
        lat: this.map.getCenter().lat(),
        lng: this.map.getCenter().lng()
      };

      resolve(mapCenterCoords);
    });
  }

  removeMarkers(){
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }



}
