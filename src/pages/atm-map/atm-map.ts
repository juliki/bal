import { Component, ElementRef, ViewChild } from '@angular/core';
import { Locations } from '../../providers/locations';
import { GoogleMaps } from '../../providers/google-maps';
import { NavController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

@Component({
  selector: 'page-atm-map',
  templateUrl: 'atm-map.html'
})
export class AtmMapPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  timeHandler: any;

  constructor(public navCtrl: NavController, public maps: GoogleMaps, public platform: Platform, public locations: Locations) {

      GoogleAnalytics.trackEvent("ATM Search Event", 'Change mode', 'Map View').then(() => {
       console.log('Map View');
      });

    this.platform.ready().then(() => {
      this.initMap();
    });

  }

  //Loads data for IOs view
  ionViewDidEnter() {
    if (this.maps.map !== undefined) {
      this.loadLocations();
      //Map watcher
      this.mapWatcher();
    }


  }

  initMap() {

    let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);

    Promise.all([
      mapLoaded
    ]).then((result) => {

      //Get ATM list for my current location (Only for initial start of map)
      this.loadLocations();

      //Map watcher
      this.mapWatcher();
    });

  }

  //If map center changes somehow, load data for coord. of the center of the map
  mapWatcher() {

    this.maps.map.addListener('center_changed', () => {

      //Clear timeout after each changes of center to prevent "bubling"
      clearTimeout(this.timeHandler);

      this.timeHandler = setTimeout(() => {

        //Load locations
        this.loadLocations();

        //Set current possitition
        this.maps.currentMapLatLng = this.maps.map.getCenter();
        this.maps.currentZoom = this.maps.map.getZoom();

      }, 1500);
    });
  }

  //Load data from server
  loadLocations() {
    //Clear existed markers
    this.maps.removeMarkers();

    if (this.maps.balOnly) {
      //Load objects for new center map position
      let locationsLoaded = this.locations.searchByLocation(true, this.maps.map.getCenter().toJSON());

      Promise.all([
        locationsLoaded
      ]).then((locations: any) => {

        console.log(locations);

        //Put markers for received locations
        for (let location of locations[0].AddressItems) {
          this.maps.addMarker(location.BasicData.Geo.YCoord, location.BasicData.Geo.XCoord, true, location.BasicData.Address);
        }


      });
    } else {

      let balLocations = this.locations.searchByLocation(true, this.maps.map.getCenter().toJSON());
      let otherLocations = this.locations.searchByLocation(false, this.maps.map.getCenter().toJSON());

      Promise.all([
        balLocations,
        otherLocations
      ]).then((locations: any) => {

        //Put markers for received locations
        //For bal
        for (let location of locations[0].AddressItems) {
          this.maps.addMarker(location.BasicData.Geo.YCoord, location.BasicData.Geo.XCoord, true, location.BasicData.Address);
        }
        //For other
        for (let location of locations[1].AddressItems) {
          this.maps.addMarker(location.BasicData.Geo.YCoord, location.BasicData.Geo.XCoord, false, location.BasicData.Address);
        }


      });

    }
  }

  changeAtm(e) {

    if (this.maps.map !== undefined) {

      //GoogleAnalytics
      let label = null;

      if (this.maps.balOnly) {
        label = "BAL only";
      } else {
        label = "BAL & Cash Group"
      }

      GoogleAnalytics.trackEvent("Map Event", 'Change search settings', label).then(() => {
        console.log(label);
      });


      this.loadLocations();
    }

  }

  getMyLocation() {
    GoogleAnalytics.trackEvent("Map Event", "Locate me").then(() => {
      console.log('Location Button Tracked');
    });

    let latLng = this.maps.getMyLocation();

    Promise.all([
      latLng
    ]).then((pan) => {
      console.log(pan);
    });
  }

}
