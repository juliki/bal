import { Component, ViewChild} from '@angular/core';
import { NavController, PopoverController, Platform, Tabs } from 'ionic-angular';
import { GoogleMaps } from '../../providers/google-maps';
import { HomeService } from '../../providers/home';
import { AtmMapPage } from '../atm-map/atm-map';
import { AtmListPage } from '../atm-list/atm-list';
import { PopoverPage } from '../popover/popover';
import { SearchResultsPage } from '../search-results/search-results';
import { Locations } from '../../providers/locations';
import { GoogleAnalytics } from 'ionic-native';

import { Events } from 'ionic-angular';


@Component({
  selector: 'page-atm-search',
  templateUrl: 'atm-search.html'
})
export class AtmSearchPage {

  tab1Root: any = AtmMapPage;
  tab2Root: any = AtmListPage;

  search = false;
  map = true;
  isIos = true;

  @ViewChild('myTabs') tabRef: Tabs;

  constructor(public navCtrl: NavController,
              public popoverCtrl: PopoverController,
              public plt: Platform,
              public maps: GoogleMaps,
              public locations: Locations,
              public homeService: HomeService,
              public events: Events
              ) {
        if (this.plt.is('ios')){
            this.isIos = true;
        } else{
            this.isIos = false;
        }

        GoogleAnalytics.trackView('ATM Search').then(() => {
         console.log('ATM Search Tracked');
       });

   }

  openSearch(){
    this.search = !this.search;
  }

  closeSearch(){
    this.search = !this.search;
  }

  toggleMap(){
  console.log(this.map);
    this.map = !this.map;
  }

  showToggle(myEvent){
    let popover = this.popoverCtrl.create(PopoverPage, {tab: this.tabRef.getSelected()});
    popover.present({
      ev: myEvent
    });
  }

  getMyLocation(){

  //tab index 0  - map, 1 - List

  let categoryName = null;

  if(this.tabRef.getSelected().index === 0){
    categoryName = 'Map Event';
  }else{
    categoryName = 'List Event';
  }

  GoogleAnalytics.trackEvent(categoryName, "Locate me").then(() => {
   console.log('Location Button Tracked');
  });

  this.maps.currentZoom = 7;

   let latLng = this.maps.getMyLocation();

    Promise.all([
      latLng
      ]).then((pan)=>{
        console.log(pan);
    });
  }

  //(Android) Opens search reults popover on searchbar focus
  searchPopover(event){
    
      //Select specific function dependent on query 

      if(this.locations.queryString === 'private mode'){

        //Check the current mode - if the same skip
        if(this.homeService.currentMode !== 'private mode'){
          this.homeService.currentMode = 'private mode';
          this.events.publish('mode:changed', true);
          this.locations.queryString = null;
        }

      }else if(this.locations.queryString === 'public mode'){

        //Check the current mode - if the same skip
        if(this.homeService.currentMode !== 'public mode'){
          this.homeService.currentMode = 'public mode';
          this.events.publish('mode:changed', true);
          this.locations.queryString = null;
        }

      }else{
        console.log("Android Search popover opens!");

        let searchResultsPopover = this.popoverCtrl.create(SearchResultsPage);

        let ev = {
            target : {
              getBoundingClientRect : () => {
                return {
                  left: '0',
                  top: '56'
                };
              }
            }
          };

        searchResultsPopover.present({
          ev
        });

      }

  }

  //(Ios) Opens serch reults popover on searchbar focus
  searchPopoverIos(event){
      //Select specific function dependent on query 

      if(this.locations.queryString === 'private mode'){

        //Check the current mode - if the same skip
        if(this.homeService.currentMode !== 'private mode'){
          this.homeService.currentMode = 'private mode';
          this.events.publish('mode:changed', true);
          this.locations.queryString = null;
        }

      }else if(this.locations.queryString === 'public mode'){

        //Check the current mode - if the same skip
        if(this.homeService.currentMode !== 'public mode'){
          this.homeService.currentMode = 'public mode';
          this.events.publish('mode:changed', true);
          this.locations.queryString = null;
        }

      }else{
        
        console.log("Ios Search popover opens!");

        let searchResultsPopover = this.popoverCtrl.create(SearchResultsPage, {}, {cssClass: 'search-popover'});

        searchResultsPopover.present({
          ev: event
        });

      }

  }

}
