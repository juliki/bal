import { Component} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Locations } from '../../providers/locations';
import { GoogleMaps } from '../../providers/google-maps';
import { GoogleAnalytics } from 'ionic-native';

@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {


  constructor(public viewCtrl: ViewController, public locations: Locations, public maps: GoogleMaps, public params: NavParams) {

      this.params = params;
   }

  close() {
    this.viewCtrl.dismiss();
  }

  changeAtm(e) {

    if (this.maps.map !== undefined) {

      this.loadLocations();
    }

  }

  //Load data from server
  loadLocations() {
    //Clear existed markers
    this.maps.removeMarkers();

    if (this.maps.balOnly) {
      //Load objects for new center map position
      let locationsLoaded = this.locations.searchByLocation(true, this.maps.map.getCenter().toJSON());

      Promise.all([
        locationsLoaded
      ]).then((locations: any) => {

        console.log(locations);

        //Put markers for received locations
        for (let location of locations[0].AddressItems) {
          this.maps.addMarker(location.BasicData.Geo.YCoord, location.BasicData.Geo.XCoord, true, location.BasicData.Address);
        }


      });
    } else {

      let balLocations = this.locations.searchByLocation(true, this.maps.map.getCenter().toJSON());
      let otherLocations = this.locations.searchByLocation(false, this.maps.map.getCenter().toJSON());

      Promise.all([
        balLocations,
        otherLocations
      ]).then((locations: any) => {

        //Put markers for received locations
        //For bal
        for (let location of locations[0].AddressItems) {
          this.maps.addMarker(location.BasicData.Geo.YCoord, location.BasicData.Geo.XCoord, true, location.BasicData.Address);
        }
        //For other
        for (let location of locations[1].AddressItems) {
          this.maps.addMarker(location.BasicData.Geo.YCoord, location.BasicData.Geo.XCoord, false, location.BasicData.Address);
        }


      });

    }
  }
}
