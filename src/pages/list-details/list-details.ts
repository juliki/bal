import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { GoogleMaps } from '../../providers/google-maps';
import { GoogleAnalytics } from 'ionic-native';

@Component({
  selector: 'page-list-details',
  templateUrl: 'list-details.html'
})
export class ListDetailsPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;

  details: Object;
  title: string;
  latitude: number;
  longitude: number;
  type: string;
  street: string;
  zip: string;
  city: string;

  constructor(public navCtrl: NavController, public maps: GoogleMaps, public params: NavParams, public platform: Platform) {
    this.platform.ready().then(() => {
      this.renderMap();
    });
  }

  ionViewDidLoad() {

    this.details = this.params.get('details');

    Promise.all([
      this.details
    ]).then((details: any) => {
      this.type = details[0].type;
      this.latitude = details[0].mainData.BasicData.Geo['YCoord'];
      this.longitude = details[0].mainData.BasicData.Geo['XCoord'];

      this.title = details[0].mainData.BasicData.Address['CompanyName'];
      this.street = details[0].mainData.BasicData.Address.Street;
      this.zip = details[0].mainData.BasicData.Address.Zip;
      this.city = details[0].mainData.BasicData.Address.City;

      //GoogleAnalytics
      GoogleAnalytics.trackView('ListDetails').then(() => {
        console.log('List Details Tracked!')
      });

      GoogleAnalytics.trackEvent("List Event", "View details", this.title + ', ' + this.street + ', ' + this.city).then(() => {
        console.log('InfoWindow open Tracked');
      });
    });

  }

  renderMap() {

    let mapLoaded = this.maps.init_details(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);

    Promise.all([
      mapLoaded,
    ]).then((result) => {

      let latLng = new google.maps.LatLng(this.latitude, this.longitude);

      this.maps.map_details.setCenter(latLng);

      this.maps.addSimpleMarker(this.latitude, this.longitude, true);

    });

  }

}
