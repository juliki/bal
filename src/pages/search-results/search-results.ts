import { Component, ViewChild} from '@angular/core';
import { ViewController } from 'ionic-angular';
import { Locations } from '../../providers/locations';
import { GoogleMaps } from '../../providers/google-maps';

import { Events } from 'ionic-angular';

@Component({
  selector: 'page-search-results',
  templateUrl: 'search-results.html'
})
export class SearchResultsPage {

  resultsList: any = [];


  constructor(public viewCtrl: ViewController, public locations: Locations, public maps: GoogleMaps, public events: Events) {

  }

  ionViewDidEnter() {
    this.loadResults();
  }

  close() {
    this.viewCtrl.dismiss();
    this.locations.queryString = null;
  }

  //Load data from server
  loadResults(){

      //Load objects for new center map position
      let resultsLoaded = this.locations.searchByQuery(this.maps.balOnly, this.locations.queryString);

      Promise.all([
        resultsLoaded
      ]).then((results: any) => {

        console.log(results[0].GeoItems);

        this.resultsList = results[0].GeoItems;


      });
  }

  selectResult(item){

    this.maps.currentMapLatLng = new google.maps.LatLng(item.LocY, item.LocX);
    this.maps.map.setCenter(this.maps.currentMapLatLng);
    this.maps.map.setZoom(13);
    console.log(item);

    //this.AtmListPage.loadLocations();
    this.events.publish('popover:closed', true);

    this.close();

  }
}
