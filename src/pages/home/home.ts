import { Component, ViewChild } from '@angular/core';
import { HomeService } from '../../providers/home';
import { Content } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;
  items = [];

  constructor(public navCtrl: NavController, public homeService: HomeService, public events: Events) {
    homeService.getMenuItems().then(
      response => {
        this.items = response.navigation_items;

        GoogleAnalytics.trackView("Home Page").then(() => {
         console.log('Home Tracked');
        });
      }
    );



    //Watch on service mode event change
    events.subscribe('mode:changed', (cchanged) => {
      homeService.getMenuItems().then(
        response => {
          this.items = response.navigation_items;
        }
      );
    });
  }

    //Loads data for IOs view
  ionViewDidEnter() {
    console.log('Home page activated! Current mode: '+ this.homeService.currentMode);
    if(this.homeService.currentMode == 'public mode'){
      this.homeService.condition = false;
    } else {
      this.homeService.condition = true;
    }
  }

  //Close opened items
  ionViewWillEnter() {
     this.events.publish('item:expanded', true);
  }

}
