import { Component, Input, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';

import { NavController, Platform } from 'ionic-angular';
import { AtmSearchPage } from '../../atm-search/atm-search';
import { IframeTemplatePage} from '../../iframe-template/iframe-template';
import { Events } from 'ionic-angular';

import { GoogleAnalytics } from 'ionic-native';

@Component({
  selector: 'page-home-item',
  templateUrl: 'home-item.html'
})
export class HomePageItem {

  @ViewChild(Content) content: Content;
  @Input() data;
  @Input() itemType;
  @Input() id;
  @Input() parent;

  // Define initial details container state
  detailsExpanded = false;

  constructor(public navCtrl: NavController, public plt: Platform, public events: Events) {

    // If item is expandable - set watcher on event 'item:expanded'
      events.subscribe('item:expanded', (expanded) => {

        if(this.detailsExpanded){
          this.detailsExpanded = !this.detailsExpanded;
          console.log(this.data.title + ' - closed');
        }
                      
      });    


  }

  //---- Expand child helpers start -----
  buttonClick(item){

      //Case for ATM search
      if(item.native_view == 'atm_search'){
        this.navCtrl.push(AtmSearchPage);
      }

      //Case have details
      if(item.details){
        //GoogleAnalytics
        let action = null;

        if (this.detailsExpanded){
          action = "Close";          
        }else{
          action = "Expand"
        }

        GoogleAnalytics.trackEvent("Home Expanding Items", action, item.title).then(() => {
         console.log('Action - '+ action + ' ' + item.title + 'Tracked');
        });

        if(!this.detailsExpanded){
          this.events.publish('item:expanded', true);
        }

        setTimeout( ()=>{
          this.detailsExpanded = !this.detailsExpanded;
          //Animation
          let yOffset = document.getElementById(this.id).offsetTop;
          console.log(yOffset);
          //console.log(this.parent);

          this.parent.scrollTo(0, yOffset - 10, 500);
          //console.log(this.parent.getContentDimensions());
        }, 50);
      }

      //Case have link
      if(item.url){
        this.navCtrl.push(IframeTemplatePage, item);
      }

  }

  //---- Expand child helpers End-----

  checkSidebarItem(item){
      return (this.plt.is('android')&& item['android-lefthand']);
  }

}
