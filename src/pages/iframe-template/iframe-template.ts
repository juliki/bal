import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

@Component({
  selector: 'page-iframe-template',
  templateUrl: 'iframe-template.html'
})
export class IframeTemplatePage {

  title = this.navParams.get('title');
  url = this.navParams.get('url');

  constructor(public navCtrl: NavController, private navParams: NavParams) {

    GoogleAnalytics.trackView(this.title).then(() => {
      console.log(this.title);
    });
  }

}
