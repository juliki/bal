import { Component } from '@angular/core';
import { NavController, Platform, App} from 'ionic-angular';
import { Locations } from '../../providers/locations';
import { ListDetailsPage } from '../list-details/list-details'
import { GoogleMaps } from '../../providers/google-maps';
import { GoogleAnalytics } from 'ionic-native';
import { Events } from 'ionic-angular';


@Component({
  selector: 'page-atm-list',
  templateUrl: 'atm-list.html'
})
export class AtmListPage {

  locationsData: any = [];
  timeHandler: any;

  constructor(public app: App, public navCtrl: NavController, public locations: Locations, public plt: Platform, public maps: GoogleMaps, public events: Events) {
    //Loads data for Android view
    if (this.maps.map !== undefined) {
      this.loadLocations();
    }

    events.subscribe('popover:closed', (closed) => {
      this.loadLocations();
    });

    GoogleAnalytics.trackEvent("ATM Search Event", 'Change mode', 'List View').then(() => {
     console.log('List View');
    });

  }

  //Loads data for IOs view
  ionViewDidEnter() {
    if (this.maps.map !== undefined) {
      this.loadLocations();
    }
  }


  pushToDetails(location) {

    if (this.plt.is('ios')) {
      let nav = this.app.getRootNav();
      nav.push(ListDetailsPage, {
        details: location
      });
    } else if (this.plt.is('android')) {
      this.navCtrl.push(ListDetailsPage, {
        details: location
      });
    }

  }

  //Load data from server
  loadLocations() {

    if (this.maps.balOnly) {
      //Load objects for new center map position
      let locationsLoaded = this.locations.searchByLocation(true, this.maps.map.getCenter().toJSON());

      Promise.all([
        locationsLoaded
      ]).then((locations: any) => {

        this.locationsData = [];

        for (let location of locations[0].AddressItems) {
          this.locationsData.push(
            {
              mainData: location,
              type: "ownAtm"
            }
          );
        }

      });
    } else {

      let balLocations = this.locations.searchByLocation(true, this.maps.map.getCenter().toJSON());
      let otherLocations = this.locations.searchByLocation(false, this.maps.map.getCenter().toJSON());

      Promise.all([
        balLocations,
        otherLocations
      ]).then((locations: any) => {

        this.locationsData = [];

        //For bal
        for (let location of locations[0].AddressItems) {
          this.locationsData.push(
            {
              mainData: location,
              type: "ownAtm"
            }
          );
        }
        //For other
        for (let location of locations[1].AddressItems) {
          this.locationsData.push(
            {
              mainData: location,
              type: "notOwnAtm"
            }
          );
        }

        //Sorting by distance

        this.locationsData.sort((a, b) => {
          if (Number(a.mainData.BasicData.Geo.Distance) > Number(b.mainData.BasicData.Geo.Distance)) {
            return 1;
          }
          if (Number(a.mainData.BasicData.Geo.Distance) < Number(b.mainData.BasicData.Geo.Distance)) {
            return -1
          }

        });


      });

    }
  }

  changeAtm(e) {

    if (this.maps.map !== undefined) {

    //GoogleAnalytics
    let label = null;

    if (this.maps.balOnly){
      label = "BAL only";
    }else{
      label = "BAL & Cash Group"
    }

    GoogleAnalytics.trackEvent("List Event", 'Change search settings', label).then(() => {
      console.log(label);
    });

      this.loadLocations();
    }

  }

  formatDistance(distance: number) {
    return (Math.round(distance / 1000 * 100) / 100);
  }


}
