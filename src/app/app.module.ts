import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { SafePipe } from '../pipes/safe.pipe';
import { SafeHtmlPipe } from '../pipes/safe-html.pipe';
import { HomePage } from '../pages/home/home';
import { HomePageItem } from '../pages/home/home-item/home-item';
import { AtmSearchPage } from '../pages/atm-search/atm-search';
import { AtmListPage } from '../pages/atm-list/atm-list';
import { AtmMapPage } from '../pages/atm-map/atm-map';
import { ListDetailsPage } from '../pages/list-details/list-details';
import { PopoverPage } from '../pages/popover/popover';
import { SearchResultsPage } from '../pages/search-results/search-results';
import { IframeTemplatePage } from '../pages/iframe-template/iframe-template';
import { Locations } from '../providers/locations';
import { GoogleMaps } from '../providers/google-maps';
import { Connectivity } from '../providers/connectivity';
import { HomeService } from '../providers/home';


@NgModule({
  declarations: [
    SafePipe,
    SafeHtmlPipe,
    MyApp,
    HomePage,
    HomePageItem,
    AtmSearchPage,
    AtmListPage,
    AtmMapPage,
    ListDetailsPage,
    IframeTemplatePage,
    PopoverPage,
    SearchResultsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Zurück'
    }, {}
  ),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomePageItem,
    AtmSearchPage,
    AtmListPage,
    AtmMapPage,
    ListDetailsPage,
    IframeTemplatePage,
    PopoverPage,
    SearchResultsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, HomeService, Locations, GoogleMaps, Connectivity]
})
export class AppModule {}
